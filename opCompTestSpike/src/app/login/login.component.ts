import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    <section class="hero is-light is-fullheight is-bold" >
      <div class="hero-body ">
        <div class="container">
          
          <div class="columns is-centered">
            <div class="column is-8-tablet is-7-desktop is-7-widescreen">
              <form action="" class="box">
                <div class="field">
                  
                  <label for="" class="label">Email</label>
                  <div class="control has-icons-left">
                    <input type="email" name="email" placeholder="e.g. bobsmith@gmail.com" class="input" required [(ngModel)]="email">
                    <span class="icon is-small is-left">
                  <i class="fa fa-envelope"></i>
                </span>
                  </div>
                </div>
                <div class="field">
                  
                  <label for="" class="label">Password</label>
                  <div class="control has-icons-left">
                    <input type="password" name="password" placeholder="*********" class="input" required [(ngModel)]="pass">
                    <span class="icon is-small is-left">
                  <i class="fa fa-lock"></i>
                </span>
                  </div>
                </div>
                <div class="field">
                  <label for="" class="checkbox">
                    
                    <input type="checkbox" name="rememberMe" [(ngModel)]="rememberMe">
                    Remember me
                  </label>
                </div>
                <div class="field">
                  <button name="login" class="button is-danger" (click)="submitForm()">
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  `,
  styles: [`
      .hero {
        background-image: url(/assets/img/doodle.jpg) !important;
        background-size: cover;
        background-position: center center;
        background-blend-mode: color-burn;
      }
    `]
})
export class LoginComponent implements OnInit {

  email:string;
  pass:string;
  rememberMe:boolean;

  constructor() { }

  ngOnInit(): void {
    //create api call
    //create the form based on pass and email
  }

  submitForm(){
    //grab all the vals and pass to server
    const emailInput = document.querySelector('input[name=email]');
    const passInput = document.querySelector('input[name=pass]');
    const rememberMeInput = document.querySelector('input[name=rememberMe]');
    alert('this is your email: '+ this.email+' this is your password: '+this.pass +' Remember you = '+ this.rememberMe);
  }

}
