import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <div class="columns">
      <div class="column m-1">
        <aside class="menu">
          <p class="menu-label">
             Pre-competition Information
          </p>
          <ul class="menu-list">
            <li><a href="https://www.msoe.edu/academics/high-school-programs/">Details & Registration</a></li>
            <li><a>Important Dates</a></li>
          </ul>
          <p class="menu-label">
            Competition Day
          </p>
          <ul class="menu-list">
            <li><a href="http://sapphire.msoe.edu:8080/OpComp/">Submission & Scoring App</a></li>
            <li><a href="https://faculty-web.msoe.edu/hasker/opcomp/rules.shtml">Competition Rules</a></li>
            <li><a href="https://faculty-web.msoe.edu/hasker/opcomp/instructions.shtml">Instructions (WiFi, IDEs, etc.)</a></li>
            <li><a href="https://faculty-web.msoe.edu/hasker/opcomp/practice.shtml">Practice Session Info</a></li>
            <li><a href="https://faculty-web.msoe.edu/hasker/opcomp/data/">Data Files</a></li>
          </ul>
        </aside>
      </div>
      <div class="column is-four-fifths">
        <section class="hero is-dark is-bold is-fullheight">
          <div class="hero-body">

            <div class="container has-text-centered ">

              <h1 class="title has-text-white">
                <a class="title has-text-white border">MSOE Op Computer Competition Homepage</a>
                <div class="m-1"></div>
                
                
                <div class="table-container m-1">
                  <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth ">
                    <!-- Your table content -->
                    <thead >
                      <th class="test">Problems (PDF)</th>
                      <th>Solutions (.java/.zip)</th>
                      <th>Scores (PDF)</th>
                      <th>Data Files</th>
                    </thead>
                    <!-- Eventually we will want to autofill this table-->
                    <tr class="is-size-4 ">
                      <th>2019</th>
                      <th>Coming soon</th>
                      <th>2019</th>
                      <th>2019</th>
                    </tr>
                    <tr class="is-size-4">
                    <th>2018</th>
                    <th>2018</th>
                    <th>2018</th>
                    <th>2018</th>
                    </tr>
                    <tr class="is-size-4">
                      <th>2017</th>
                      <th>2017</th>
                      <th>2017</th>
                      <th>2017</th>
                    </tr>
                    <tr class="is-size-4">
                      <th>2016</th>
                      <th>2016</th>
                      <th>2016</th>
                      <th>2016</th>
                    </tr>
                    <tr class="is-size-4">
                      <th>2015</th>
                      <th>2015</th>
                      <th>2015</th>
                      <th>2015</th>
                    </tr>
                    <tr class="is-size-4">
                      <th>2014</th>
                      <th>2014</th>
                      <th>2014</th>
                      <th>2014</th>
                    </tr>
                    <tr class="is-size-4">
                      <th>2013</th>
                      <th>2013</th>
                      <th>2013</th>
                      <th>2013</th>
                    </tr>
                    <tr class="is-size-4">
                      <th>2003</th>
                      <th>2003</th>
                      <th>2003</th>
                      <th>2003</th>
                    </tr>
                    
                  </table>
                </div>
              </h1>
            </div>
          </div>
        </section>
      </div>

    </div>


    
  `,
  styles: [`
      .hero {
        background-image: url(/assets/img/hasker.jpg) !important;
        background-size: cover;
        background-position: center top;
        text-decoration-color: white;
      }


    `]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
